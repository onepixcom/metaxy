(function($){
  $(document).ready(function(){
    if( $(".metaxy_tab_title").length ){
      let allTabTitles = []
      let allTabContent = []

      $(".metaxy_tab_title").each(function(){
        let $tabTitle = $(this),
        $tabContent = $tabTitle.next(".form-table")
        if( allTabTitles[allTabTitles.length-1] ){
          allTabTitles[allTabTitles.length-1].after( $tabTitle )
        }
        allTabTitles.push( $tabTitle )
        allTabContent.push( $tabContent )
        
        $tabTitle.on("click", function(){
          allTabTitles.forEach(function( item ){ item.removeClass('active') })
          allTabContent.forEach(function( item ){ item.hide() })
          $tabContent.show()
          $tabTitle.addClass("active")
        })
      })
      allTabTitles[0].click();
    }
  });
})(jQuery)