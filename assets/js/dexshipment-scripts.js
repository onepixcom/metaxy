(function($){
  	$(document).ready(function(){
	  	$('#shipment-tracking-button').on('click', function(e){
	  		e.preventDefault();
	  		let order_id = $(this).attr('data-order_id');
	  		$.ajax({
		        type: 'POST',
		        dataType: 'json',
		        url: MyAjax.ajaxurl,
		        data: {                
		        	action: 'get_shipment_tracking',
	                order_id: order_id,  
	            },
	            beforeSend: function (data) {
		        	
		    	},
		        success: function (data) {
		        	$('.shipping-tracking-responce').html(data.html);
		    	}
		    }); 
	  	});

	  	$('#create-manifest').on('click', function(e){
			e.preventDefault();
	  		let courier = $(this).attr('data-courier');
	  		$.ajax({
		        type: 'POST',
		        dataType: 'json',
		        url: MyAjax.ajaxurl,
		        data: {                
		        	action: 'create_manifest',
	                courier: courier, 
	            },
	            beforeSend: function (data) {
		        	
		    	},
		        success: function (data) {
		        	
		        	if( data.responce.Manifest ){
		        		let $base64 = data.responce.Manifest;
		        		var byteCharacters = atob( $base64 );
			            var byteNumbers = new Array(byteCharacters.length);
			            
			            for (var i = 0; i < byteCharacters.length; i++) {
			            	byteNumbers[i] = byteCharacters.charCodeAt(i);
			            }

			            var byteArray = new Uint8Array(byteNumbers);
			            var file = new Blob([byteArray], { type: 'application/pdf;base64' });
			            var fileURL = URL.createObjectURL(file);

			            var a = document.createElement("a");
			            a.href = fileURL;
			            a.text = 'Download Manifest';
			            a.className = 'create-manifest-button';
			            a.target = '_blank';
			            $('#create-manifest').replaceWith(a);

		        	} else {
		        		$('#create-manifest-button-wrap').prepend('<p>Shipments are not ready to be shipped.</p>');
		        	}
		        	/*$('.confirm-payment-responce').html(data.html);*/
		    	}
		    }); 
	  	});

  	});
})(jQuery)