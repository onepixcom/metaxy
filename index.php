<?php
/* 
 * Plugin Name: WooCommerce Metaxy Payment Gateway
 * Description: Metaxy Payment Gateway for WooCommerce
 * Author: OnePix
 * Author URI : one-pix.com
 * Version 1.0.1
 * Text Domain: metaxy_gateway
 */

add_filter( 'query_vars', function( $query_vars ) {
  $query_vars[] = 'wc-order';
  return $query_vars;
} );

add_filter('woocommerce_payment_gateways', 'metaxy_add_gateway_class');
function metaxy_add_gateway_class($gateways)
{
  $gateways[] = 'WC_Metaxy_Gateway';
  return $gateways;
}

/*
 * Init Payment Gateway when plugins have already been loaded
 */
add_action('plugins_loaded', 'metaxy_init_gateway_class');
function metaxy_init_gateway_class()
{
  if( !class_exists( 'WC_Payment_Gateway' ) ) return false;

  load_plugin_textdomain( 'metaxy_gateway', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

  class WC_Metaxy_Gateway extends WC_Payment_Gateway
  {

    /**
     * Add Payment Gateway parameters
     */
    public function __construct()
    {

      $this->id = 'metaxy';
      $this->has_fields = true;
      $this->method_title = esc_html__( 'Metaxy Gateway', 'metaxy_gateway' );
      $this->method_description = esc_html__( 'Metaxy Payment Gateway', 'metaxy_gateway' );

      // gateways can support subscriptions, refunds, saved payment methods,
      $this->supports = array(
        'products',
        'refunds'
      );

      // Method with all the options fields
      $this->init_form_fields();

      // Load the settings.
      $this->init_settings();
      $this->title = $this->get_option('title');
      $this->description = $this->get_option('description');
      $this->enabled = $this->get_option('enabled');

      $this->payment_action = intval( $this->get_option('payment_action') );
      $this->payment_type = intval( $this->get_option('payment_type') );

      $this->owner_type = $this->get_option('owner_type');
      
      $this->order_prefix = $this->get_option('order_prefix');
      $this->cf_number = $this->get_option('cf_number');
      $this->vat_number = $this->get_option('vat_number');

      $this->testmode = 'yes' === $this->get_option('testmode');
      

      $this->brand = $this->testmode ? $this->get_option('test_brand') : $this->get_option('live_brand');
      $this->warehouse = $this->testmode ? $this->get_option('test_warehouse') : $this->get_option('live_warehouse');

      $this->apikey = $this->testmode ? $this->get_option('test_metaxy_apikey') : $this->get_option('live_metaxy_apikey');
      $this->password = $this->testmode ? $this->get_option('test_metaxy_password') : $this->get_option('live_metaxy_password');

      // This action hook saves the settings
      add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
      add_filter( 'woocommerce_available_payment_gateways', array( $this, 'metaxy_gateway_disable_conditions' ));
      
      // URL for responce after GenerateOrderToken
      add_action( 'woocommerce_api_wc_metaxy_payment_gateway', array( $this, 'webhook' ) ); 
      add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
    }

    // Add notice if credentials are empty
    public function noconfig_notice()
    {
?>
      <div class="notice is-dismissible notice-alert">
        <p><?php echo esc_html__('Metaxy Gateway cannot work without credentials', 'metaxy_gateway' ); ?></p>
      </div>
<?php
    }

    /**
     * @param array $available_gateways All Payment Gateways
     * 
     * @return array $available_gateways Array with Availaible Payment Gateways 
     */

    // hide Metaxy Payment Gateway on the checkout page if shipping  methos is not ALL, SDA, SGT, DHL, SkyNet
    public function metaxy_gateway_disable_conditions( $available_gateways ) {

      if ( ! is_admin() ) {
            
          $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
            
          $chosen_shipping = $chosen_methods[0];
            
          if ( isset( $available_gateways['metaxy'] ) && 0 !== strpos( $chosen_shipping, 'metaxy_all' ) && 0 !== strpos( $chosen_shipping, 'metaxy_sda' ) && 0 !== strpos( $chosen_shipping, 'metaxy_sgt' ) && 0 !== strpos( $chosen_shipping, 'metaxy_dhl' ) && 0 !== strpos( $chosen_shipping, 'metaxy_skynet' ) ) {

             unset( $available_gateways['metaxy'] );

          }
            
      }
      
      return $available_gateways;

    }

    /**
     * @param bool $auth_array Return header array?
     * 
     * @return string|array Base64 encoded key for authorization or array
     */
    public function do_auth( $auth_array = true )
    {
      
      $credentials = $this->apikey . ':' . $this->password;

      $http_key = base64_encode( $credentials );

      if( $auth_array ){

        return [
          'Authorization' => 'Basic ' . $http_key,
          'Content-Type' => 'application/json'
        ];

      } else {

        return $http_key;

      }

    } 

    /**
     * @param string $method Gateway method
     * 
     * @return string URL for request
     */
    public function get_route( $method )
    {
      
      if( $this->testmode ){

        $base_url = 'http://test.services.dataexpert.it/api/'. $this->brand .'/V1/' . $method;

      } else {
        
        $base_url = 'https://services.dataexpert.it/api/'. $this->brand .'/V1/' . $method;
        
      }

      

      return $base_url;

    }


    /**
     * @param string $method Gateway method
     * 
     * @return string URL for Manifest request
     */

    public function get_manifest_route( $method )
    {
      
      if( $this->testmode ){

        $base_url = 'http://test.services.dataexpert.it/api/Warehouse/'. $this->warehouse .'/V1/' . $method;

      } else {
        
        $base_url = 'https://services.dataexpert.it/api/Warehouse/'. $this->warehouse .'/V1/' . $method;
        
      }

      return $base_url;

    }

    /**
     * Plugin options
     */
    public function init_form_fields()
    {

      $this->form_fields = array(
        'enabled' => array(
          'title'       => esc_html__('Enable/Disable', 'metaxy_gateway' ),
          'label'       => esc_html__('Enable Metaxy Gateway', 'metaxy_gateway' ),
          'type'        => 'checkbox',
          'description' => '',
          'default'     => 'no'
        ),

        'title' => array(
          'title'       => esc_html__('Title', 'metaxy_gateway' ),
          'type'        => 'text',
          'description' => esc_html__('This controls the title which the user sees during checkout.', 'metaxy_gateway' ),
          'default'     => esc_html__('Pay with Metaxy', 'metaxy_gateway' ),
          'desc_tip'    => true,
        ),
        'description' => array(
          'title'       => esc_html__('Description', 'metaxy_gateway' ),
          'type'        => 'textarea',
          'description' => esc_html__('This controls the description which the user sees during checkout.', 'metaxy_gateway' ),
          'default'     => esc_html__('Pay with your credit card via Metaxy payment gateway.', 'metaxy_gateway' ),
        ),
        
        'testmode' => array(
          'title'       => esc_html__('Test mode', 'metaxy_gateway' ),
          'label'       => esc_html__('Enable Test Mode', 'metaxy_gateway' ),
          'type'        => 'checkbox',
          'default'     => 'yes',
          'desc_tip'    => true,
        ),

        'credentials_tab' => array(
          'title'       => esc_html__('Shop Data', 'metaxy_gateway' ),
          'type'        => 'title',
          'class'       => 'metaxy_tab_title',
        ),

        'order_prefix' => array(
          'title'       => esc_html__('Orders prefix', 'metaxy_gateway' ),
          'type'        => 'text',
          'default'     => 'wp-metaxy-',
        ),

        'payment_action' => array(
          'title'       => esc_html__('Payment Action', 'metaxy_gateway' ),
          'type'        => 'select',
          'options'     => [
            '1' => esc_html__('Authorize', 'metaxy_gateway' ),
            '2' => esc_html__('Capture', 'metaxy_gateway' ),
            ]
          ),
        'payment_type' => array(
          'title'       => esc_html__('Payment Type', 'metaxy_gateway' ),
          'type'        => 'select',
          'options'     => [
            '1' => esc_html__('Paypal', 'metaxy_gateway' ),
            '2' => esc_html__('Credit Card', 'metaxy_gateway' ),
            '6' => esc_html__('Transfer', 'metaxy_gateway' ),
          ]
        ),
        
        

        'owner_type' => array(
          'title'       => esc_html__('Shop owner type', 'metaxy_gateway' ),
          'type'        => 'select',
          'options'     => [
            'private' => esc_html__('Private', 'metaxy_gateway' ),
            'company' => esc_html__('Company', 'metaxy_gateway' )
          ]
        ),

        'cf_number' => array(
          'title'       => esc_html__('CF Number', 'metaxy_gateway' ),
          'type'        => 'text',
        ),
        'vat_number' => array(
          'title'       => esc_html__('VAT Number', 'metaxy_gateway' ),
          'type'        => 'text',
        ),


        'test_tab' => array(
          'title'       => esc_html__('Test Credentials', 'metaxy_gateway' ),
          'type'        => 'title',
          'class'       => 'metaxy_tab_title',
        ),
        'test_brand' => array(
          'title'       => esc_html__('Test Brand', 'metaxy_gateway' ),
          'type'        => 'text',
        ),

        'test_warehouse' => array(
          'title'       => esc_html__('Test Ship Company (for creating Manifest)', 'metaxy_gateway' ),
          'type'        => 'text',
        ),

        'test_metaxy_apikey' => array(
          'title'       => esc_html__('Test Api Key', 'metaxy_gateway' ),
          'type'        => 'text',
        ),
        'test_metaxy_password' => array(
          'title'       => esc_html__('Test Password', 'metaxy_gateway' ),
          'type'        => 'text',
        ),

        'live_tab' => array(
          'title'       => esc_html__('Live Credentials', 'metaxy_gateway' ),
          'type'        => 'title',
          'class'       => 'metaxy_tab_title',
        ),
        'live_brand' => array(
          'title'       => esc_html__('Live Brand', 'metaxy_gateway' ),
          'type'        => 'text',
        ),

        'live_warehouse' => array(
          'title'       => esc_html__('Live Ship Company (for creating Manifest)', 'metaxy_gateway' ),
          'type'        => 'text',
        ),

        'live_metaxy_apikey' => array(
          'title'       => esc_html__('Live Api Key', 'metaxy_gateway' ),
          'type'        => 'text',
        ),
        'live_metaxy_password' => array(
          'title'       => esc_html__('Live Password', 'metaxy_gateway' ),
          'type'        => 'text',
        ), 
      );
    }

    /**
     * Add Payment Gateway decription
     */
    public function payment_fields()
    {
      // ok, let's display some description before the payment form
      if ($this->description) {
        // you can instructions for test mode, I mean test card numbers etc.
        if ($this->testmode) {

          $this->description .= __(' TEST MODE ENABLED.', 'metaxy_gateway' );
          
        }
        // display the description with <p> tags etc.
        echo wpautop(wp_kses_post($this->description));
      }
      
    }

    // Scripts in the Dashboard 
    public function admin_scripts(){

      wp_enqueue_script( 'metaxy_js_admin', plugins_url( 'assets/js/metaxy-back.js' , __FILE__ ), array( 'jquery' ) );
      wp_enqueue_style( 'metaxy_css_admin', plugins_url( 'assets/css/metaxy-back.css', __FILE__ ) );

    }
    

    /*
		 * We're processing the payments here
		 */
    public function process_payment($order_id)
    {

      $order = wc_get_order($order_id);

      if ( empty( $order )) {
          
        wc_add_notice( esc_html__("Cannot find this order", 'metaxy_gateway' ), 'error');

        return ['result' => 'failure'];

      }

      /* 
        Start of generating data for GeneratePaymentToken 
      */
      $product_list = [];
      $_tax = new WC_Tax();
      $total_discount = 0;
      $total_price = 0;
      $volume = 0;
      $weight = 0;
      $sku_list = array();

      //Get data of each product in the order
      foreach ( $order->get_items() as $key => $item) {
        $sku_list_item = array();
        
        $product = $item->get_product();

        $rates_percents = [];
        if( $product->get_tax_status() == 'taxable' ){

          $rates = $_tax->get_rates($product->get_tax_class()); // get all taxes for product
          $rates_percents = array_column( $rates, 'rate' ); // get array with only values of percents

        }

        if( empty( $rates_percents ) ){
          $rates_percents = [0];
        }
        
        $discount = empty( $product->get_sale_price() ) ? 0 : round( $product->get_regular_price(), 2 ) - round( $product->get_sale_price(), 2 );
        $quantity = intval($item->get_quantity());

        $product_tax = $item->get_subtotal_tax();
        $price = number_format( $product->get_regular_price(), 2 ) + number_format( $product_tax / $quantity, 2 );
        $round_price = number_format($price, 2);
        $product_list[] = [
          "SKU"         => $product->get_sku(),
          "Description" => $product->get_name(),
          "Price"       => $round_price,
          "VATRates"    => $rates_percents,
          "Quantity"    => $quantity,
          "Discount"    => $discount,
        ];
        $total_price .= $round_price;

        $width          = $product->get_width();
        $height         = $product->get_height();
        $depth          = $product->get_length();
        $product_weight = $product->get_weight();
        $volume         += $width * $height * $depth * $quantity;
        $weight         += $product_weight * $quantity;

        $sku_list_item['SKU'] = $product->get_sku();
        $sku_list_item['Quantity'] = $quantity;
        $sku_list_item['RefundReasonIT'] = '';
        $sku_list_item['RefundReasonIT'] = '';

        array_push( $sku_list, $sku_list_item );
        
      }


      $notification_url = home_url() . '/wc-api/wc_metaxy_payment_gateway';
      $order_data = $order->get_data();

      $total_price .= number_format( $order->get_shipping_total(), 2 ) + number_format( $order->get_shipping_tax(), 2 );

      $payment_token_data = [
        "OrderNumber" => $this->order_prefix . $order->ID,
        "ProductList" => $product_list,
        "Currency" => $order->get_currency(),
        "TotalAmount" => $order->get_total(),
        "TotalDiscount" => $total_discount,
        "ShippingAddress" => [
          "BusinessName" => $order_data['shipping']['company'],
          "Address" => $order_data['shipping']['address_1'],
          "City" => $order_data['shipping']['city'],
          "Zip" => $order_data['shipping']['postcode'],
          "Province" => $order_data['shipping']['state'],
          "CountryCode" => $order_data['shipping']['country'],
          "Email" => $order_data['billing']['email'],
        ],
        "BillingAddress" => [
          "BusinessName" => $order_data['billing']['company'],
          "Address" => $order_data['billing']['address_1'],
          "City" => $order_data['billing']['city'],
          "Zip" => $order_data['billing']['postcode'],
          "Province" => $order_data['billing']['state'],
          "CountryCode" => $order_data['billing']['country'],
          "Name" => $order_data['billing']['first_name'],
          "Surname" => $order_data['billing']['last_name'],
          "PecEmail" => $order_data['billing']['email'],
          "Email" => $order_data['billing']['email'],
        ],
        "ResponseToMerchantUrl" => $notification_url,
        "RecoveryUrl" => $order->get_checkout_order_received_url(),
        "OrderCountryCode" => WC()->countries->get_base_country(),
        "Note" => $order_data['customer_note'],
        "PaymentAction" => $this->payment_action,
        "PaymentType" => $this->payment_type,
        "InvoiceCallbackEndpoint" => $order->get_view_order_url(),
      ];
      
      if( $this->owner_type == 'private' ){

        $payment_token_data['BillingAddress']["CF"] = $this->cf_number; 

      } else {
        
        $payment_token_data['BillingAddress']["VATNumber"] = $this->vat_number;

      }

      if( floatval( $order->get_shipping_total() ) > 0 ){

        $shipping_rates_percents = [];
        
        $shipping_taxes = $_tax->get_shipping_tax_rates();
        $shipping_rates_percents = array_column( $shipping_taxes, 'rate' );
        if( empty( $shipping_rates_percents ) ){
          $shipping_rates_percents = [0];
        }

        $shipment_amount_with_tax = floatval( $order->get_shipping_total() ) + floatval( $order->get_shipping_tax() );
        $payment_token_data["ShipmentAmount"] = $shipment_amount_with_tax;
        $payment_token_data["ShipmentType"] = $order->get_shipping_method();
        $payment_token_data["ShipmentVatRates"] = $shipping_rates_percents;

      }
      /* 
        End of generating data for GeneratePaymentToken 
      */


      // GeneratePaymentToken request
      $payment_token_request = wp_remote_post( $this->get_route('GeneratePaymentToken'), [
        'headers' => $this->do_auth(),
        'body' => json_encode( $payment_token_data ),
      ] );

      // Show notice if GeneratePaymentToken request is failed
      if ( is_wp_error( $payment_token_request ) ) {

        $error_message = $payment_token_request->get_error_message();

        wc_add_notice(  esc_html__( $error_message, 'metaxy_gateway' ) , 'error');

        return ['result' => 'failure'];

      }

      $response = json_decode( $payment_token_request['body'], true );
      
      // Update order meta if GeneratePaymentToken request is successful
      if( $response['ResponseStatus']['Success'] ){

          foreach ($response as $key => $value) {
            update_post_meta( $order->ID, $key, $value );
          }

        $order->set_transaction_id( $response['PaymentId'] );
        $order->update_status('wc-pending');

        // Save order meta 'order_payment_type' if Metaxy Payment Gateway type is bank transfer  
        if( $this->payment_type == 6):

          update_post_meta( $order->ID, 'order_payment_type', 'transfer');

        endif;

        //Redirect to HostedPageUrl after successful GeneratePaymentToken
        return ['result' => 'success', 'redirect' => $response['HostedPageUrl']];
        
      } else {

        // Show notice if GeneratePaymentToken request is failed
        wc_add_notice( esc_html__( $response['ResponseStatus']['ErrorMessage'], 'metaxy_gateway' )); 
        
        return ['result' => 'failure'];

      }

    }

    /*
		 * Webhook for responce after GeneratePaymentToken
		 */
    public function webhook()
    { 

      $args = array(
        'post_type'   => 'shop_order',
        'post_status' => 'any',
        'meta_query'  => array(
          'relation'  => 'AND',
          array(
            'key'     => 'PaymentId',
            'value'   => $_REQUEST['PaymentId'],
            'compare' => '='
          ),
          array(
            'key'     => 'SecurityToken',
            'value'   => $_REQUEST['SecurityToken'],
            'compare' => '='
          )
        ),
      );
      $orders = new WP_Query( $args );
      if ( $orders->have_posts() ) {
        while ( $orders->have_posts() ) { $orders->the_post();
          $order_id = get_the_ID();
        }
        wp_reset_postdata();
      }

      // Get order in processing
      $order = wc_get_order($order_id);

      // Update order note
      $order->add_order_note(  esc_html__( 'Order paid via Metaxy.', 'metaxy_gateway' ), 1 );

      $order->payment_complete( $_REQUEST['PaymentId'] );

      // Redirect to 'Order Received' page 
      echo $order->get_checkout_order_received_url();
      die;
    }


    /**
     * @param number|string $order_id Order ID
     * @param number|null $amount Refund amount
     * @param string $reason Refund reason
     * 
     * @return bool Refund or not
     */

    public function process_refund( $order_id, $amount = null, $reason = '' ) {

      $order = wc_get_order($order_id);

      /* Start of generating data for refund */
      $items = $order->get_items();

      $sku_list = array();
        
      foreach ($items as $key => $item) {

        $sku_list_item = array();

        $product        = $item->get_product();
        $quantity       = intval( $item->get_quantity() );

        $sku_list_item['SKU'] = $product->get_sku();
        $sku_list_item['Quantity'] = $quantity;
        $sku_list_item['RefundReasonIT'] = '';
        $sku_list_item['RefundReasonIT'] = '';

        array_push( $sku_list, $sku_list_item );

      }

      $refund_order_data = array(
          "OrderNumber"   => $this->order_prefix . $order_id,
          "SKUList"       => $sku_list,
          "RMANumber"     => ""
      ); 
      /* End of generating data for refund */

      // Refund request
      $refund_order_request = wp_remote_post( $this->get_route('MakeRefund'), [
          'timeout' => 20,
          'headers' => $this->do_auth(),
          'body' => json_encode( $refund_order_data ),
      ] );

      // Update Order meta with refund responce
      update_post_meta( $order_id, 'refund_order_responce', json_encode( $refund_order_request ));

      //Refund order in WooCommerce if responce successful
      $refund_order_response = $refund_order_request['body'];
      if ( $refund_order_response['ResponseStatus']['Success'] ) {
        return true;
      }
    }

  }
}

/*
  Class Metaxy_Order_Shipment for generating Tax documents, creating Manifest, generating Order Shipment, getting Shipment Tracking
*/
class Metaxy_Order_Shipment {
  public function __construct () {
    add_action( 'woocommerce_product_options_shipping', array( $this,'add_custom_shipping_option_to_products' ) );
    add_action( 'woocommerce_process_product_meta', array( $this, 'save_custom_shipping_option_to_products' ) );
    add_action( 'admin_enqueue_scripts', [$this, 'register_dexshipment_admin_assets'] );
    
    // Adding metaboxes on the Order page in the Dashboard
    add_action( 'add_meta_boxes',  [$this, 'order_shipment_metabox'] );
    add_action( 'add_meta_boxes',  [$this, 'tax_documents_metabox'] );
    add_action( 'add_meta_boxes',  [$this, 'shipment_tracking_metabox'] );
    add_action( 'add_meta_boxes',  [$this, 'create_manifest_metabox'] );

    add_action( 'wp', [ $this, 'generate_order_shipment_handler' ] ); 
    
    // Hooks add AJAX hadler to get Shipment tracking
    add_action('wp_ajax_nopriv_get_shipment_tracking',  array( $this, 'get_shipment_tracking_handler') );
    add_action('wp_ajax_get_shipment_tracking',  array( $this, 'get_shipment_tracking_handler') );

    add_action('wp_ajax_nopriv_create_manifest',  array( $this, 'create_manifest_handler') );
    add_action('wp_ajax_create_manifest',  array( $this, 'create_manifest_handler') );
  }

  // Function gets Shipment Tracking
  public function get_shipment_tracking_handler() {
    
    if(isset($_POST['order_id'])) {
      $order_id = $_POST['order_id'];
    }

    $html = '';

    // Get Metaxy Payment Gateway data
    $payment_gateway = WC()->payment_gateways->payment_gateways()['metaxy'];

    // Get Metaxy Payment Gateway data
    $shipment_tracking_request = wp_remote_get( trim($payment_gateway->get_route( 'GetShipmentTracking/' . $payment_gateway->order_prefix . $order_id)), [
        'timeout' => 20,
        'headers' => $payment_gateway->do_auth(),
    ] );


    // Shipment Tracking request
    $shipment_tracking_responce = json_decode( $shipment_tracking_request['body'], true );
   
    //Get Shipment Tracking events
    $events = $shipment_tracking_responce['Events'];
    
    $html .= '<ul>';
    if(!empty($events)):
      
      foreach ($events as $key => $value) {
        $html .= '<li><span>' . $key . ':</span>' . $value . '</li>';
      }

    else:
      $html .= '<li>No events found</li>';
    endif;
    $html .= '</ul>';

    // Send Shipment Tracking enents to the the Front-end
    wp_send_json(array(
      'html'  => $html,
    ));
  }

  // Add new options for products, that used in GenerateOrderShipment
  public function add_custom_shipping_option_to_products(){
    global $post, $product;


    echo '</div><div class="options_group">'; // New option group

    woocommerce_wp_select( 
      array( 
        'id'          => '_is_dangerous', 
        'label'       => __( 'Is Dangerous?', 'metaxy_gateway' ),
        'desc_tip'    => 'true', 
        'description' => __( 'Choose product dangerous status.', 'metaxy_gateway' ),
        'value'       => get_post_meta( $post->ID, '_is_dangerous', true ),
        'options' => array(
          'no'   => __( 'No', 'metaxy_gateway' ),
          'yes'   => __( 'Yes', 'metaxy_gateway' ),
        )
      )
    );

    woocommerce_wp_text_input( array(
        'id'          => '_dangerous_class',
        'label'       => __( 'Dangerous class', 'metaxy_gateway' ),
        'placeholder' => '',
        'desc_tip'    => 'true',
        'description' => __( 'Enter the dangerous class here.', 'metaxy_gateway' ),
        'value'       => get_post_meta( $post->ID, '_dangerous_class', true ),
    ) );
  }

  // Save new options for products, that used in GenerateOrderShipment
  public function save_custom_shipping_option_to_products( $post_id ){

    $is_dangerous = $_POST['_is_dangerous'];
    if( isset( $is_dangerous ) )
        update_post_meta( $post_id, '_is_dangerous', esc_attr( $is_dangerous ) );

    $dangerous_class = $_POST['_dangerous_class'];
    if( isset( $dangerous_class ) )
        update_post_meta( $post_id, '_dangerous_class', esc_attr( $dangerous_class ) );
  }


  // Register scripts and styles
  public function register_dexshipment_admin_assets() {
    global $pagenow;
    wp_register_style('dexshipment-styles', plugins_url('assets/css/dexshipment-style.css', __FILE__));
    wp_register_script('dexshipment-scripts', plugins_url('assets/js/dexshipment-scripts.js', __FILE__), array('jquery'), null, false);

    wp_localize_script('dexshipment-scripts', 'MyAjax',    array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
    $current_page = get_current_screen();
    if (( $pagenow == 'post.php' ) && ($current_page->post_type == 'shop_order')) {
        wp_localize_script('dexshipment-scripts', 'Order', array( 
            'id' => get_the_ID(),
        ) );
    }
    wp_enqueue_style('dexshipment-styles');
    wp_enqueue_script('dexshipment-scripts');        
  }


  /*

    Start of adding metaboxes on the Order page in the Dashboard 

  */
  public function order_shipment_metabox(){
    add_meta_box( 'metaxy_order_shipment_metabox', __('Generate Order Shipment','metaxy_gateway'), [$this, 'add_order_shipment_generate_button'], 'shop_order', 'side', 'core' );
  }

  private function order_shipment_generate_button( $order_id ) {
    $html = '<div class="dexshipment-generate-wrap" data-order_id="' . $order_id . '"></div>';
    return $html;
  }

  // Function generates Order shipment button from order's  generated meta
  public function add_order_shipment_generate_button() {
      global $post;
      echo $this->order_shipment_generate_button($post->ID);
      $shipment_response = json_decode(get_post_meta($post->ID, 'shipping_responce', true));
      $base64 = $shipment_response->Shipment->Pdf;
      if($base64):
?>
      <script type="text/javascript">
        var byteCharacters = atob(<?php echo "'" . $base64 . "'"; ?>);
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        var file = new Blob([byteArray], { type: 'application/pdf;base64' });
        var fileURL = URL.createObjectURL(file);

        var a = document.createElement("a");
        a.href = fileURL;
        a.text = 'Download Order Shipment';
        a.className = 'dexshipment-generate';
        a.target = '_blank';
        jQuery('.dexshipment-generate-wrap').append(a);
      </script>
<?php 
    else:
      echo __('Order shipment isn`t generated', 'metaxy_gateway');
    endif;
  }

  public function tax_documents_metabox(){
    add_meta_box( 'metaxy_tax_documents_metabox', __('Tax documents','metaxy_gateway'), [$this, 'add_tax_documents_generate_buttons'], 'shop_order', 'side', 'core' );
  }

  // Function generates Tax docunents buttons from order's  generated meta 
  public function add_tax_documents_generate_buttons() {
      global $post;
      $tax_documents_response = json_decode(get_post_meta($post->ID, 'generate_tax_documents_response', true));
      $documents = $tax_documents_response->Documents;

      $document_name = array(
        '1' => __('Invoice', 'metaxy_gateway'),
        '2' => __('Credit Note', 'metaxy_gateway'),
        '3' => __('Receipt', 'metaxy_gateway'),
        '4' => __('Receipt Canceled', 'metaxy_gateway'),
      );

      if ( is_array( $documents ) ) {
        foreach ($documents as $document ) {
          echo '<div id="doc-' . $document->DocumentNumber . '" class="document-link-wrap"></div>';
          $base64 = $document->Pdf; ?>
          <script type="text/javascript">
            var byteCharacters = atob(<?php echo "'" . $base64 . "'"; ?>);
            var byteNumbers = new Array(byteCharacters.length);
            for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            var file = new Blob([byteArray], { type: 'application/pdf;base64' });
            var fileURL = URL.createObjectURL(file);

            var a = document.createElement("a");
            a.href = fileURL;
            a.text = 'Download <?= $document_name[$document->DocumentTypology]; ?> (<?= $document->DocumentNumber; ?>)';
            a.className = 'document-link';
            a.target = '_blank';
            jQuery('#doc-<?= $document->DocumentNumber; ?>').append(a);
          </script>
    <?php 
        }   
      } else {
        echo __("Documents weren't generated", 'metaxy_gateway');
      }
  }  

  public function shipment_tracking_metabox(){
    add_meta_box( 'metaxy_shipment_tracking_metabox', __('Get Shipment Tracking','metaxy_gateway'), [$this, 'add_shipment_tracking_button'], 'shop_order', 'side', 'core' );
  }

  // Function add button for receiving shipment tracking events
  public function add_shipment_tracking_button(){
    $order_id = get_the_ID();
    echo '<a href="#" id="shipment-tracking-button" data-order_id="' . $order_id . '">Get shipment tracking</a>';
    echo '<div class="shipping-tracking-responce"></div>';
  }

  public function create_manifest_metabox(){
    add_meta_box( 'metaxy_create_manifest_metabox', __('Create Manifest','metaxy_gateway'), [$this, 'add_create_manifest_button'], 'shop_order', 'side', 'core' );
  }

  // Function generates CreateManifest from order's generated meta 
  public function add_create_manifest_button(){
     global $post;
     $order = wc_get_order($post->ID);
          echo '<div id="create-manifest-button-wrap"></div>';
          $create_manifest = json_decode(get_post_meta($post->ID, 'create_manifest_generated', true));
          $base64 = $create_manifest->Manifest;
          if($base64):
    ?>
          <script type="text/javascript">
            var byteCharacters = atob(<?php echo "'" . $base64 . "'"; ?>);
            var byteNumbers = new Array(byteCharacters.length);
            for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            var file = new Blob([byteArray], { type: 'application/pdf;base64' });
            var fileURL = URL.createObjectURL(file);

            var a = document.createElement("a");
            a.href = fileURL;
            a.text = 'Download Manifest';
            a.className = 'create-manifest-button';
            a.target = '_blank';
            jQuery('#create-manifest-button-wrap').append(a);
          </script>
    <?php 
        else:
          echo '<a href="#" id="create-manifest" data-courier="' . strtoupper(substr( reset( $order->get_items( 'shipping' ) )->get_method_id() , 7)) . '">Create Manifest</a>';
        endif;
  }
  /*

    End of adding metaboxes on the Order page in the Dashboard 

  */

  // CreateManifest handler
  public function create_manifest_handler(){
    
    if( isset($_POST['courier']) ){
      $courier = $_POST['courier'];
      $create_manifest_data  = [
          "Courier" => $courier
      ];

      // Get Metaxy Payment Gateway data
      $payment_gateway = WC()->payment_gateways->payment_gateways()['metaxy'];

      //CreateManifest request
      $create_manifest_request = wp_remote_post( trim($payment_gateway->get_manifest_route( 'CreateManifest' )), [
        'timeout' => 20,  
        'headers' => $payment_gateway->do_auth(),
        'body' => json_encode( $create_manifest_data ),
      ] );

      $create_manifest_response = json_decode( $create_manifest_request['body'], true );

      // save CreateManifest responce to order meta
      wp_send_json(array(
        'responce'  =>  $create_manifest_response,
      ));

    }

  }

  // GenerateOorderShipment handler
  public function generate_order_shipment_handler() {
    
    // check if is 'Order Received page'
    if ( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ) {

      global $wp;
      $order_id  = absint( $wp->query_vars['order-received'] );
      $order = wc_get_order($order_id);

      // Get Metaxy Payment Gateway data
      $payment_gateway = WC()->payment_gateways->payment_gateways()['metaxy'];
      
      // get Order's courier
      $courier = strtoupper(substr( reset( $order->get_items( 'shipping' ) )->get_method_id() , 7));

      if ( empty($order_id) || $order_id == 0 ) {
          return;
      } 

      //Check if Order Shipment wasn't generated
      $order_shipment_generated = get_post_meta($order_id, 'order_shipment_generated', true);

      if ( $order_shipment_generated != 'generated' ) {
        
        /* 
          Start of generating data for Order Shipment request
        */
        $items = $order->get_items();

        $volume = 0;
        $weight = 0;

        $sku_list = array();

        $is_dangerous = 'False';
        $dangerous_class = '';
          
        foreach ($items as $key => $item) {

          $sku_list_item      = array();
          $product_id         = $item->get_product_id();
          $product            = $item->get_product();
          $width              = $product->get_width();
          $height             = $product->get_height();
          $depth              = $product->get_length();
          $quantity           = intval( $item->get_quantity() );
          $product_weight     = $product->get_weight();
          $volume             += $width * $height * $depth * $quantity;
          $weight             += $product_weight * $quantity;
          $is_dangerous_value = get_post_meta($product_id, '_is_dangerous', true);
          
          if ( $is_dangerous_value == 'yes' ) {
            $is_dangerous = 'True';
          }

          if ( $is_dangerous_value == 'yes' ) {
            $dangerous_class_value = get_post_meta($product_id, '_dangerous_class', true);
            $dangerous_class .= $dangerous_class_value . ', ';
          }
          
          $sku_list_item['SKU'] = $product->get_sku();
          $sku_list_item['Quantity'] = $quantity;
          $sku_list_item['RefundReasonIT'] = '';
          $sku_list_item['RefundReasonIT'] = '';

          array_push( $sku_list, $sku_list_item );

        }

        $dex_shipment_data  = [
          "OrderNumber"     => $payment_gateway->order_prefix . $order_id,
          "PackageWeight"   => $weight,
          "PackageHeight"   => number_format( pow($volume, 1/3), 2),
          "PackageWidth"    => number_format( pow($volume, 1/3), 2),
          "PackageDepth"    => number_format( pow($volume, 1/3), 2),
          "IsDangerous"     => $is_dangerous,
          "DangerousClass"  => $dangerous_class,
          "Courier"         => $courier
        ];
        /* 
          End of generating data for Order Shipment request
        
        */

        // Order Shipment request
        $dex_shipment_request = wp_remote_post( $payment_gateway->get_route('GenerateOrderShipment'), [
          'headers' => $payment_gateway->do_auth(),
          'body' => json_encode( $dex_shipment_data ),
        ] );

        $shipment_response = json_decode( $dex_shipment_request['body'], true );

        //Save Order Shipment responce 
        update_post_meta( $order_id, 'shipping_responce', json_encode($shipment_response));

        /*
          If Order Shipment responce is successful and Order's payment type != bank transfer 
          start to generate data for CaptureOrderAmount request
        */
        if ( $shipment_response['ResponseStatus']['Success'] && $payment_gateway->payment_type != 6) {

            // Data for CaptureOrderAmount request
            $capture_order_amount_data = array(
                "OrderNumber"   => $payment_gateway->order_prefix . $order_id,
                "SKUList"       => $sku_list,
                "PackageWeight" => $weight,
                "PackageHeight" => number_format( pow($volume, 1/3), 2),
                "PackageWidth"  => number_format( pow($volume, 1/3), 2),
                "PackageDepth"  => number_format( pow($volume, 1/3), 2)
            ); 

            // CaptureOrderAmount request
            $capture_order_amount_request = wp_remote_post( $payment_gateway->get_route('CaptureOrderAmount'), [
                'timeout' => 20,
                'headers' => $payment_gateway->do_auth(),
                'body' => json_encode( $capture_order_amount_data ),
            ] );

            // save CaptureOrderAmount responce to the order meta
            update_post_meta( $order_id, 'capture_order_amount_responce', json_encode( $capture_order_amount_request ));                

            $capture_order_amount_responce = json_decode( $capture_order_amount_request['body'], true );


            /*
              If CaptureOrderAmount responce is successful
              start to generate data for GetTaxDocuments request
            */
            if( $capture_order_amount_responce['ResponseStatus']['Success'] ):

              // Data for GetTaxDocuments request
              $generate_tax_documents_data = array(
                  "OrderNumber"   => $payment_gateway->order_prefix . $order_id,
              ); 

              // GetTaxDocuments request
              $generate_tax_documents_request = wp_remote_get( trim($payment_gateway->get_route( 'GetTaxDocuments/' . $payment_gateway->order_prefix . $order_id)), [
                  'timeout' => 20,
                  'headers' => $payment_gateway->do_auth(),
              ] );

              $generate_tax_documents_request = json_decode( $generate_tax_documents_request['body'], true );

              // save GetTaxDocuments responce to the order meta
              update_post_meta( $order_id, 'generate_tax_documents_response', json_encode( $generate_tax_documents_request )); 
            endif;

            // Update order meta if OrderShipment was generated
            update_post_meta($order_id, 'order_shipment_generated', 'generated');
        }
        
      }

      /* 
        Check if Metaxy Payment Gateway payment type was 
        'Bank transfer' start to generate data for ConfirmPayment request 
      */
      $order_payment_type = get_post_meta($order_id, 'order_payment_type', true);

      if($order_payment_type && $order_payment_type == 'transfer') {

        // Data for ConfirmPayment request 
        $security_token = get_post_meta($order_id, 'SecurityToken', true);
        $payment_id = get_post_meta($order_id, 'PaymentId', true);

        $confirm_payment_data = array(
          "OrderNumber"   => $payment_gateway->order_prefix . $order_id,
          "SecurityToken" => $security_token,
          "DexPaymentId"  => $payment_id,
          "PaymentType"   => "Transfer"
        );

        //ConfirmPayment request 
        $confirm_payment_request = wp_remote_post( $payment_gateway->get_route('ConfirmPayment'), [
          'headers' => $payment_gateway->do_auth(),
          'body' => json_encode( $confirm_payment_data ),
        ] );

        $confirm_payment_responce = json_decode( $confirm_payment_request['body'], true );

        // save ConfirmPayment responce to the order meta
        update_post_meta( $order_id, 'confirm_payment_responce', json_encode( $confirm_payment_request ));
      
        /*
          If ConfirmPayment responce is successful
          start to generate data for GetTaxDocuments request
        */
        if( $confirm_payment_responce['ResponseStatus']['Success'] ):

          //Data for GetTaxDocuments request
          $generate_tax_documents_data = array(
              "OrderNumber"   => $payment_gateway->order_prefix . $order_id,
          ); 

          //GetTaxDocuments request
          $generate_tax_documents_request = wp_remote_get( trim($payment_gateway->get_route( 'GetTaxDocuments/' . $payment_gateway->order_prefix . $order_id)), [
              'timeout' => 20,
              'headers' => $payment_gateway->do_auth(),
          ] );

          $generate_tax_documents_request = json_decode( $generate_tax_documents_request['body'], true );

          // save GetTaxDocuments responce to the order meta
          update_post_meta( $order_id, 'generate_tax_documents_response', json_encode( $generate_tax_documents_request )); 
        endif;     
      }

      //Check if Manifest wasn't generated  
      $create_manifest_generated = get_post_meta($order_id, 'create_manifest_generated', true);

      if( !$create_manifest_generated ) {


        // Data for CreateManifest request
        $create_manifest_data  = [
          "Courier" => $courier
        ];

        //CreateManifest request 
        $create_manifest_request = wp_remote_post( trim($payment_gateway->get_manifest_route( 'CreateManifest' )), [
          'timeout' => 20,  
          'headers' => $payment_gateway->do_auth(),
          'body' => json_encode( $create_manifest_data ),
        ] );

        $create_manifest_response = json_decode( $create_manifest_request['body'], true );

        // save CreateManifest responce to the order meta
        update_post_meta( $order_id, 'create_manifest_generated', json_encode($create_manifest_response));

      }

    }

  }

}
// Metaxy_Order_Shipment Class init
new Metaxy_Order_Shipment();

/*
  End of Class Metaxy_Order_Shipment
*/


/* Add Metaxy shipping methods for Payment Gateway */
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {


  /* Add ALL shipping method */
  function metaxy_shipping_methods_init() {

    if ( ! class_exists( 'WC_Metaxy_ALL_Shipping_Method' ) ) {
      class WC_Metaxy_ALL_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
          $this->id                 = 'metaxy_all'; // Id for your shipping method. Should be uunique.
          $this->method_title       = __( 'Metaxy ALL Shipping Method', 'metaxy_gateway' );  // Title shown in admin
          $this->method_description = __( 'Metaxy ALL Shipping Method', 'metaxy_gateway' ); // Description shown in admin

          $this->enabled            = "yes"; // This can be added as an setting but for this example its forced enabled
          $this->title              = "ALL"; // This can be added as an setting but for this example its forced.

          $this->init();
        }

        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
          $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
          $rate = array(
            'label' => $this->title,
            'cost' => '00.00',
            'calc_tax' => 'per_item'
          );

          // Register the rate
          $this->add_rate( $rate );
        }
      }
    }

    if ( ! class_exists( 'WC_Metaxy_SGT_Shipping_Method' ) ) {
      class WC_Metaxy_SGT_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
          $this->id                 = 'metaxy_sgt'; // Id for your shipping method. Should be uunique.
          $this->method_title       = __( 'Metaxy SGT Shipping Method', 'metaxy_gateway' );  // Title shown in admin
          $this->method_description = __( 'Metaxy SGT Shipping Method', 'metaxy_gateway' ); // Description shown in admin

          $this->enabled            = __("yes", 'metaxy_gateway'); // This can be added as an setting but for this example its forced enabled
          $this->title              = __("SGT Corriere", 'metaxy_gateway'); // This can be added as an setting but for this example its forced.

          $this->init();
        }

        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
          $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
          $rate = array(
            'label' => $this->title,
            'cost' => '00.00',
            'calc_tax' => 'per_item'
          );

          // Register the rate
          $this->add_rate( $rate );
        }
      }
    }

        if ( ! class_exists( 'WC_Metaxy_SDA_Shipping_Method' ) ) {
      class WC_Metaxy_SDA_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
          $this->id                 = 'metaxy_sda'; // Id for your shipping method. Should be uunique.
          $this->method_title       = __( 'Metaxy SDA Shipping Method', 'metaxy_gateway' );  // Title shown in admin
          $this->method_description = __( 'Metaxy SDA Shipping Method', 'metaxy_gateway' ); // Description shown in admin

          $this->enabled            = __("yes", 'metaxy_gateway'); // This can be added as an setting but for this example its forced enabled
          $this->title              = __("SDA Express Courier", 'metaxy_gateway'); // This can be added as an setting but for this example its forced.

          $this->init();
        }

        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
          $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
          $rate = array(
            'label' => $this->title,
            'cost' => '00.00',
            'calc_tax' => 'per_item'
          );

          // Register the rate
          $this->add_rate( $rate );
        }
      }
    }

        if ( ! class_exists( 'WC_Metaxy_DHL_Shipping_Method' ) ) {
      class WC_Metaxy_DHL_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
          $this->id                 = 'metaxy_dhl'; // Id for your shipping method. Should be uunique.
          $this->method_title       = __( 'Metaxy DHL Shipping Method', 'metaxy_gateway' );  // Title shown in admin
          $this->method_description = __( 'Metaxy DHL Shipping Method', 'metaxy_gateway' ); // Description shown in admin

          $this->enabled            = __("yes", 'metaxy_gateway'); // This can be added as an setting but for this example its forced enabled
          $this->title              = __("DHL Express", 'metaxy_gateway'); // This can be added as an setting but for this example its forced.

          $this->init();
        }

        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
          $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
          $rate = array(
            'label' => $this->title,
            'cost' => '00.00',
            'calc_tax' => 'per_item'
          );

          // Register the rate
          $this->add_rate( $rate );
        }
      }
    }

        if ( ! class_exists( 'WC_Metaxy_SKYNET_Shipping_Method' ) ) {
      class WC_Metaxy_SKYNET_Shipping_Method extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
          $this->id                 = 'metaxy_skynet'; // Id for your shipping method. Should be uunique.
          $this->method_title       = __( 'Metaxy SKYNET Shipping Method', 'metaxy_gateway' );  // Title shown in admin
          $this->method_description = __( 'Metaxy SKYNET Shipping Method', 'metaxy_gateway' ); // Description shown in admin

          $this->enabled            = __("yes", 'metaxy_gateway'); // This can be added as an setting but for this example its forced enabled
          $this->title              = __("SkyNet-Italy", 'metaxy_gateway'); // This can be added as an setting but for this example its forced.

          $this->init();
        }

        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
          // Load the settings API
          $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
          $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

          // Save settings in admin if you have any defined
          add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
          $rate = array(
            'label' => $this->title,
            'cost' => '00.00',
            'calc_tax' => 'per_item'
          );

          // Register the rate
          $this->add_rate( $rate );
        }
      }
    }
  }

  add_action( 'woocommerce_shipping_init', 'metaxy_shipping_methods_init' );

  function add_metaxy_shipping_methods( $methods ) {
    $methods['metaxy_all']    = 'WC_Metaxy_ALL_Shipping_Method';
    $methods['metaxy_sgt']    = 'WC_Metaxy_SGT_Shipping_Method';
    $methods['metaxy_sda']    = 'WC_Metaxy_SDA_Shipping_Method';
    $methods['metaxy_dhl']    = 'WC_Metaxy_DHL_Shipping_Method';
    $methods['metaxy_skynet'] = 'WC_Metaxy_SKYNET_Shipping_Method';
    return $methods;
  }

  add_filter( 'woocommerce_shipping_methods', 'add_metaxy_shipping_methods' );
}
/* End of adding Metaxy shipping methods */
